# -*- coding: utf-8 -*-
import os
import re
import numpy
import sklearn
import random
import pickle
import codecs
import unicodedata
from ichiYoshiBig import buildIndicesList, createPaddedList, split_unicode_chrs,  tokenizeByMecabWrapper
from sklearn.model_selection import StratifiedKFold
from gensim.models import word2vec
from keras.models import Sequential, Model
from keras.models import load_model
from keras.layers.convolutional import Conv1D
from keras.layers.pooling import MaxPooling1D
from keras.layers import Merge
from keras.layers.merge import Concatenate, concatenate
from keras.layers import Activation, Dense, Dropout, Embedding, Flatten, Input, LSTM, Bidirectional
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from keras.optimizers import SGD
from keras.utils import plot_model
from flask import Flask, render_template, flash,  request, make_response
from flask.ext.wtf import Form
from wtforms import TextAreaField, SubmitField
from wtforms.validators import DataRequired
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from werkzeug.utils import secure_filename

#choose an action first
#ACTION = "train & cross-validate"
ACTION = "predict"

#interim output from creating vocabulary is  written out as files and read again to avoid interruption by memory errors

#load vocab
with open("vocabInvBigTok", mode='rb') as f:
    vocabInvTok = pickle.load(f)
with open("vocabBigTok", mode='rb') as f:
    vocabTok = pickle.load(f)
with open("vocabInvBigChar", mode='rb') as f:
    vocabInvChar = pickle.load(f)
with open("vocabBigChar", mode='rb') as f:
    vocabChar = pickle.load(f)

#add a symbol for representing all unknown words to the vocab
vocabInvTok.append("♩")
vocabTok["♩"] = len(vocabInvTok) - 1
vocabInvChar.append("♩")
vocabChar["♩"] = len(vocabInvChar) - 1


#load pre-trained character embedding due to time constraint
charVecModel = word2vec.Word2Vec.load("20features_1minwords_10contextC")
tokVecModel = word2vec.Word2Vec.load("20features_1minwords_10contextT")

#interim output from turning charaters into indices is  written out as files and read againto avoid interruption by memory errors

#load indexed training data (mini set to speed up debugging)
#posTokIndicesListFile = open("posIndicesListBigTokMin.txt", "r")
#negTokIndicesListFile = open("negIndicesListBigTokMin.txt", "r")
#posCharIndicesListFile = open("posIndicesListBigCharMin.txt", "r")
#negCharIndicesListFile = open("negIndicesListBigCharMin.txt", "r")

#load indexed training data (full set)
posTokIndicesListFile = open("posIndicesListBigTok.txt", "r")
negTokIndicesListFile = open("negIndicesListBigTok.txt", "r")
posCharIndicesListFile = open("posIndicesListBigChar.txt", "r")
negCharIndicesListFile = open("negIndicesListBigChar.txt", "r")

#positive negative example ratio used for training and validation
pNRatio = 0.5

#set to 1 due to time constraint
numEpoch = 1

stepsPerEpoch = 5

#k-fold cross-validation
k = 5

modelName = "tokCharVec"

#loop over training data to determine:
#   input dimension for neural network: sampleLength
#   step size used when feeding data to neural network: step size

#possible to skip when training with a dataset a second time after finding out the above parameters from the dataset in a previous session
#when skipped, replace with definition of step size and input dimension

#start skip
posSamplesTok = posTokIndicesListFile.readlines()
posSampleSize = len(posSamplesTok)
print ("positive token sample size:" + str(posSampleSize))
negSampleSize = len(negTokIndicesListFile.readlines())
print ("negative token sample size:" + str(negSampleSize))
sampleLengthTok = len(posSamplesTok[0].split())
print ("token sample length:" + str(sampleLengthTok))
posSamplesChar = posCharIndicesListFile.readlines()
sampleLengthChar = len(posSamplesChar[0].split())
print("char sample length:" + str(sampleLengthChar))

#ratio of positve example vs negative example are adjusted. hence the adjusted sample size
if posSampleSize >= negSampleSize:
    adjSampleSize = (k - 1) * posSampleSize / k / pNRatio
elif negSampleSize > posSampleSize:
    adjSampleSize = (k - 1) * negSampleSize / k / (1 - pNRatio)
stepSize = int(adjSampleSize / stepsPerEpoch)
print ("step size:" + str(stepSize))

negCharIndicesListFile.close()
posCharIndicesListFile.close()
negTokIndicesListFile.close()
posTokIndicesListFile.close()

#reload indexed training data (mini set to speed up debugging)
#posTokIndicesListFile = open("posIndicesListBigTokMin.txt", "r")
#negTokIndicesListFile = open("negIndicesListBigTokMin.txt", "r")
#posCharIndicesListFile = open("posIndicesListBigCharMin.txt", "r")
#negCharIndicesListFile = open("negIndicesListBigCharMin.txt", "r")

#reload indexed training data (full set)
posTokIndicesListFile = open("posIndicesListBigTok.txt", "r")
negTokIndicesListFile = open("negIndicesListBigTok.txt", "r")
posCharIndicesListFile = open("posIndicesListBigChar.txt", "r")
negCharIndicesListFile = open("negIndicesListBigChar.txt", "r")
#end skip

app = Flask(__name__, template_folder='')
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'well-secret-password'

class MyForm(Form):
    input = TextAreaField(label='Paste a sell side report here:')
    send = SubmitField(label='Send pasted text')

def loadDataKfold(tokIndicesListFile,charIndicesListFile,sign, k):
    tokIndicesLines = tokIndicesListFile.readlines()
    charIndicesLines = charIndicesListFile.readlines()
    xT = []
    xC = []
    y = []
    for tokIndicesLine, charIndicesLine  in zip(tokIndicesLines, charIndicesLines):
        tokIndices = [int(i) for i in tokIndicesLine.split()]
        charIndices = [int(i) for i in charIndicesLine.split()]
        xT.append(tokIndices)
        xC.append(charIndices)
        y.append(sign)
    folds = list(StratifiedKFold(n_splits=k, shuffle=True, random_state=1).split(xT, y))
    return folds, numpy.array(xT), numpy.array(xC)

def adjustRatio(xTPlusValidCv, xTMinusValidCv, xCPlusValidCv, xCMinusValidCv, pNRatio):
    xTStore = []
    xCStore = []
    yStore = []
    xTPlus4Pop = xTPlusValidCv.tolist()
    xTMinus4Pop = xTMinusValidCv.tolist()
    xCPlus4Pop = xCPlusValidCv.tolist()
    xCMinus4Pop = xCMinusValidCv.tolist()
    while 1:
        if random.random() > pNRatio:
            if not len(xTMinus4Pop) == 0:
                indicesT = xTMinus4Pop.pop()
                indicesC = xCMinus4Pop.pop()
                label = 0
            else:
                break
        else:
            if not len(xTPlus4Pop) == 0:
                indicesT = xTPlus4Pop.pop()
                indicesC = xCPlus4Pop.pop()
                label = 1
            else:
                break
        xTStore.append(indicesT)
        xCStore.append(indicesC)
        yStore.append(label)
    return [numpy.array(xTStore), numpy.array(xCStore)], numpy.array(yStore)

#feed data to neural network while keeping the ratio of positive examples to negative examples at pNRatio
def generateArraysFromLists(xTPlus,xTMinus,xCPlus, xCMinus, pNRatio,stepSize):
    stepCount = 0
    xTStore = []
    xCStore = []
    yStore = []
    xTPlus4Pop = xTPlus.tolist()
    xTMinus4Pop = xTMinus.tolist()
    xCPlus4Pop = xCPlus.tolist()
    xCMinus4Pop = xCMinus.tolist()
    while 1:
        if random.random() > pNRatio:
            if not len(xTMinus4Pop) == 0:
                indicesT = xTMinus4Pop.pop()
                indicesC = xCMinus4Pop.pop()
                label = 0
            else:
                xTMinus4Pop = xTMinus.tolist()
                xCMinus4Pop = xCMinus.tolist()
                indicesT = xTMinus4Pop.pop()
                indicesC = xCMinus4Pop.pop()
                label = 0
        else:
            if not len(xTPlus4Pop) == 0:
                indicesT = xTPlus4Pop.pop()
                indicesC = xCPlus4Pop.pop()
                label = 1

            else:
                xTPlus4Pop = xTPlus.tolist()
                xCPlus4Pop = xCPlus.tolist()
                indicesT = xTPlus4Pop.pop()
                indicesC = xCPlus4Pop.pop()
                label = 1
        xTStore.append(indicesT)
        xCStore.append(indicesC)
        yStore.append(label)
        stepCount += 1
        if stepCount == stepSize:
            stepCount = 0
            xTOut = numpy.array(xTStore)
            xCOut = numpy.array(xCStore)
            xTStore = []
            xCStore = []
            yOut = numpy.array(yStore)
            yStore = []
            yield [xTOut,xCOut], yOut

#the model takes a concatenation of the token embedding and characer embedding of the input text
#the character embedding would help when the token embedding is having trouble with unknown words
def createModel(vocabTok, vocabInvTok, vocabChar, vocabInvChar, tokVecModel, charVecModel, sampleLengthTok, sampleLengthChar):
    embedding_dim = 20
    dropout_prob = (0.25, 0.5)
    hidden_dims = 200
    tokEmbeddingWeights = [numpy.array([tokVecModel[w] if w in tokVecModel \
                                          else numpy.random.uniform(-0.25, 0.25, tokVecModel.vector_size) \
                                      for w in vocabInvTok])]

    charEmbeddingWeights = [numpy.array([charVecModel[w] if w in charVecModel \
                                          else numpy.random.uniform(-0.25, 0.25, charVecModel.vector_size) \
                                      for w in vocabInvChar])]
    tokEmbedLayer = Sequential()
    tokEmbedLayer.add(Embedding(len(vocabTok), embedding_dim, input_length=sampleLengthTok, weights=tokEmbeddingWeights))
    tokEmbedLayer.add(Bidirectional(LSTM(10, return_sequences=True)))
    charEmbedLayer = Sequential()
    charEmbedLayer.add(Embedding(len(vocabChar), embedding_dim, input_length=sampleLengthChar, weights=charEmbeddingWeights))
    charEmbedLayer.add(Bidirectional(LSTM(10, return_sequences=True)))
    model = Sequential()
    model.add(Merge([tokEmbedLayer, charEmbedLayer], mode='concat', concat_axis=1))
    model.add(Dense(hidden_dims))
    model.add(Dropout(dropout_prob[1]))
    model.add(Flatten())
    model.add(Activation('relu'))
    model.add(Dense(1))
    model.add(Activation('sigmoid'))
    opt = SGD(lr=0.01, momentum=0.80, decay=1e-6, nesterov=True)
    model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['accuracy'])
    plot_model(model, to_file="arch.png", show_shapes=True)
    return model

def processReport4Predict(pasted, lines4PredictFile):
    lines4Predict = []
    for l in re.split("\n|。", pasted):
        if not l.startswith("http") and not l.strip() == "":
            lines4PredictFile.write(l.rstrip()+"\n")
    return

@app.route('/', methods=['GET', 'POST'])
def index():
    form = MyForm()
    model = load_model("model" + modelName + ".hdf5")
    if form.validate_on_submit():
        paddedTokListFile = codecs.open("paddedTokList4PredictBig.txt", mode="w", encoding="utf8")
        paddedCharListFile = codecs.open("paddedCharList4PredictBig.txt", mode="w", encoding="utf8")
        tokIndicesListFile = open("tokIndicesList4PredictBig.txt", "w")
        charIndicesListFile = open("charIndicesList4PredictBig.txt", "w")
        lines4PredictFile = codecs.open("lines4PredictBig.txt", mode="w", encoding="utf8")
        processReport4Predict(unicodedata.normalize('NFKC', form.input.data).rstrip(), lines4PredictFile)
        lines4PredictFile.close()
        lines4PredictFile = codecs.open("lines4PredictBig.txt", mode="r", encoding="utf8")
        paddedTokLists, paddedCharLists = createPaddedList(lines4PredictFile, sampleLengthTok, sampleLengthChar, paddedTokListFile, paddedCharListFile, [], [])
        tokIndicesLists = buildIndicesList(vocabTok, paddedTokLists, tokIndicesListFile, sampleLengthTok, "")
        charIndicesLists = buildIndicesList(vocabChar, paddedCharLists, charIndicesListFile, sampleLengthChar, "")
        xT = []
        for indicesLine in tokIndicesLists:
           indices = [int(i) for i in indicesLine.split()]
           xT.append(indices)
        xC = []
        for indicesLine in charIndicesLists:
           indices = [int(i) for i in indicesLine.split()]
           xC.append(indices)
# return average score of lines in the submitted report
        score = numpy.mean(numpy.array(model.predict([numpy.array(xT),numpy.array(xC)])))
        if  score >= 0.5:
            result = "long"
        else:
            result = "short or do nothing"
        if form.send.data:
            flash("{posted}".format(posted=str(result)))
            return render_template('predictForm.html', form=form)

        lines4PredictFile.close()
        paddedTokListFile.close()
        paddedCharListFile.close()
        tokIndicesListFile.close()
        charIndicesListFile.close()
    return render_template('predictForm.html', form=form)

if __name__ == '__main__':
    if  ACTION == "train & cross-validate":
        model = createModel(vocabTok, vocabInvTok, vocabChar, vocabInvChar, tokVecModel, charVecModel, sampleLengthTok, sampleLengthChar)
#model check points for saving best session and applying early stopping when further training does not improving results.
#not needed in  a test environment when epoch is set to 1
        mC = ModelCheckpoint("model"+modelName+".{epoch:02d}.hdf5", monitor="loss", mode="auto", save_best_only=True,
                             save_weights_only=False)
        eS = EarlyStopping(monitor='loss', patience=10, verbose=0, mode='auto')
#split positive examples into training and validation set by k-1 to 1
        posFolds, xTPlus, xCPlus = loadDataKfold(posTokIndicesListFile, posCharIndicesListFile, 1, k)
#split negative examples into training and validation set by k-1 to 1
        negFolds, xTMinus, xCMinus = loadDataKfold(negTokIndicesListFile, negCharIndicesListFile, 0, k)
        for i, (posTrainValIdx,  negTrainValIdx) in enumerate(zip(posFolds, negFolds)):
            print('\nFold ', i)
            posTrainIdx = posTrainValIdx[0]
            posValIdx = posTrainValIdx[1]
            negTrainIdx = negTrainValIdx[0]
            negValIdx = negTrainValIdx[1]
            xTPlusTrainCv = xTPlus[posTrainIdx]
            xTPlusValidCv = xTPlus[posValIdx]
            xTMinusTrainCv = xTMinus[negTrainIdx]
            xTMinusValidCv = xTMinus[negValIdx]
            xCPlusTrainCv = xCPlus[posTrainIdx]
            xCPlusValidCv = xCPlus[posValIdx]
            xCMinusTrainCv = xCMinus[negTrainIdx]
            xCMinusValidCv = xCMinus[negValIdx]
#adjust ratio of positive examples and negative examples in the validation set based on pNRatio
            xValidCv, yValidCv = adjustRatio(xTPlusValidCv, xTMinusValidCv, xCPlusValidCv, xCMinusValidCv, pNRatio)
            model.fit_generator(
                generateArraysFromLists(xTPlusTrainCv,xTMinusTrainCv,xCPlusTrainCv,xCMinusTrainCv, pNRatio,stepSize),
                steps_per_epoch=stepsPerEpoch,
                shuffle=True,
                epochs=numEpoch,
                verbose=1,
                callbacks=[eS, mC],
                validation_data = (xValidCv, yValidCv))
            print(model.evaluate(xValidCv, yValidCv))
        model.model.save("model"+modelName+".hdf5", overwrite=True)

    elif ACTION == "predict":
#prediction comes with a gui for pasting reports
        app.run(host='0.0.0.0', port=5008)

    negCharIndicesListFile.close()
    posCharIndicesListFile.close()
    negTokIndicesListFile.close()
    posTokIndicesListFile.close()
