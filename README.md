#README

##Objective 

Predicting whether a sell-side report from brokers leads to a long recommendation from analysts

##Data Description

The original data used (*ideasheet_test.csv*  and *ichiyoshi.mbox*) when writing the codes here are not available for the public. Interested readers can replace *ichiyoshi.mbox* with a collection of mails from brokers with reports. The subject field of these reports need to have a stock symbol. *ideasheet_test.csv* has three rows, namely, date, symbol, recommendation(LLL/L/S/<empty>). 

##Overall Description

Two alternatives are presented here. Both are neural-based. The first alternative, made up by the files *ichiYoshi.py* and *hayate.py*, is meant to be simple. The second alternative, made up by the files *ichiYoshiBig.py* and *hayateBig.py*, is mean to be more ambitous and showcase something more fancy that can be done.

##Assumption Made by the First Alternative in Data Preparation (*ichiYoshi.py*) 

Only reports about symbols covered in *ideasheet_test.csv* and released before the date recommendations are made in *ideasheet_test.csv* are used in training and classified into positive/negative examples

##Assumption Made by the Second Alternative in Data Preparation (*ichiYoshiBig.py*)

Reports about symbols not covered in *ideasheet_test.csv* are used as negative examples in training. Reports about symbols covered in ideasheet_test.csv and but released after the date recommendations are made in *ideasheet_test.csv* are also used as negative examples in training

##Differences between the First Alternative and the Second Alternative  

###First Alternative
*   input embedded in a character vector space model
*   the neural network used is a bi-lstm that accepts an input sentence as a 111x20 2D vector
*   unknown characters treated as spaces
*   the prediction part accepts a text file put in a path specified in the program *hayate.py*

###Second Alternative
*   input embeded in a character vector space model and a word vector space model
*   the neural network used is a bi-lstm that accepts an input sentence in two forms, as a 55x20 2D vector and a 102x20 2D vector. The former is the representation of the sentence in the word vector space model. The latter is the representation of the sentence in the character space model. The two representations are concatenated and fed to the neural network.
*   a symbol for unknown word/token is added to the vocabulary to represent all unknown words/characters
*   the prediction part comes with a gui that let users paste a report for prediction. 

##Remarks on Coding Convention
As a habit, I write out the result of any major steps to files and read from the files again. I am fully aware that this is clumpsy and much slower than keeping the data in memory. But it gives me the advantage of resuming from every major step when the whole program is interrupted by memory errors, which frequenly occur in programming with libraries made for deep learning. The time saved from reading from memory instead of files often comes with an unjustified risk of losing the time used in creating the data read. 

##File Description


*  *ichiYoshi.py*
part of the first alternative. process the provided raw data into a form that can be used for training a neural network. 

*  *hayate.py*
part of the first alternative. train a neural network using the data processed by *ichiYoshi.py* or predict from a text file with a trained model

*  *ichiYoshiBig.py*
part of the second alternative. process the provided raw data into forms that can be used for training a neural network

*  *hayateBig.py*
part of the second alternative. train a neural network using the data processed by *ichiYoshiBig.py* or predict from text pasted to a gui with a trained model

* *predictForm.html*
part of the second alternative. the template of the form for users to paste text during prediction 

##Instruction

### Common Instructions

*   set up an appropriate environment with the following commands in ubuntu


```
    apt-get install python3 
    apt-get install python3-pip 
    apt-get install emacs 
    pip3 install theano==0.9 
    pip3 install sklearn
    pip3 install -I keras ==2.1.5
    pip3 install gensim 
    pip3 install h5py 
    pip3 install -I flask==0.12.2
    pip3 install Flask-WTF
    apt-get install swig
    apt-get install mecab
    apt-get install mecab-naist-jdic
    apt-get install libmecab-dev
    apt-get install mecab-ipadic-utf8
    apt-get install python-mecab　　　　
    pip3 install mecab-python3
    apt-get install python3.5-dev
    pip3 install JapaneseTokenizer
    pip3 install neologd 
    apt-get install graphviz 
    pip3 install pydot
```


*   set "backend" to "theano" in *.keras/keras.json*　　　　

*   download *20features_1minwords_10contextC*  and *20features_1minwords_10contextT* from 103.63.133.33 (id:testAcct/pw:hayateIchiyoshi) and put them in the installation folder

*   put *ichiyoshi.mbox* and *ideasheet_test.csv* in the installation directory

### Instructions for Alternative One

#### Training


*   run *ichiYoshi.py*

*   look for the following line in *hayate.py*. If it starts with a "#" remove it.

```
ACTION = "train & cross-validate"


```
*   look for the following line in *hayate.py*. If it does not start with a "#", put a "#" before it.

```
ACTION = "predict"


```

*   looks for the following line in *hayate.py*. Replace "/home/labmanager/extraData/modelsCPlusCW24/20features_1minwords_10contextC" with the path where you store the downloaded file *20features_1minwords_10contextC*.

```
charVecModel = word2vec.Word2Vec.load("/home/labmanager/extraData/modelsCPlusCW24/20features_1minwords_10contextC")
```

*   run *hayate.py*

#### Prediction

*   name the input for prediction *input4PredictLine.txt* and put it in the installation folder

*   look for the following line in *hayate.py*. If it does not start with a "#", put a "#" before it. 

```
ACTION = "train & cross-validate"


```
*   look for the following line in *hayate.py*. If it starts with a "#" remove it.

```
ACTION = "predict"
```

*   run *hayate.py*

### Instructions for Alternative Two

#### Training

*   run *ichiYoshiBig.py*

*   look for the following line in *hayateBig.py*. If it starts with a "#" remove it.

```
ACTION = "train & cross-validate"


```
*   look for the following line in *hayateBig.py*. If it does not start with a "#", put a "#" before it.

```
ACTION = "predict"


```

*   looks for the following line in *hayateBig.py*. Replace "/home/labmanager/extraData/modelsCPlusCW24/20features_1minwords_10contextC" with the path where you store the downloaded file *20features_1minwords_10contextC* .
```
charVecModel = word2vec.Word2Vec.load("/home/labmanager/extraData/modelsCPlusCW24/20features_1minwords_10contextC")
```
*   looks for the following line in *hayateBig.py*. Replace "/home/labmanager/extraData/modelsCPlusCW24/20features_1minwords_10contextT" with the path where you store the downloaded file *20features_1minwords_10contextT*.
```
tokVecModel = word2vec.Word2Vec.load("/home/labmanager/extraData/modelsCPlusCW24/20features_1minwords_10contextT")
```

*   run *hayateBig.py*


#### Prediction

*   look for the following line in *hayateBig.py*. If it does not start with a "#", put a "#" before it. 

```
ACTION = "train & cross-validate"


```
*   look for the following line in *hayateBig.py*. If it starts with a "#" remove it.

```
ACTION = "predict"
```

*   run *hayateBig.py*

*   launch a brower and put "<your ip address>:5008" in the address bar

*   paste the input for prediciton in the space provided and press "Send pasted text"


