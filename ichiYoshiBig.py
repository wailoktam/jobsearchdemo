import mailbox
import codecs
import email
import re
import numpy
import JapaneseTokenizer
import unicodedata
import itertools
import pickle

from email.utils import parsedate
from time import mktime,strptime
from email.header import decode_header
from collections import Counter
from itertools import chain

def removeKey(oldDict, key):
    newDict = dict(oldDict)
    del newDict[key]
    return newDict

def split_unicode_chrs(text):
    pat_unicode_chr_splitter = re.compile('(?s)((?:[\ud800-\udbff][\udc00-\udfff])|.)').split
    return [ chr for chr in pat_unicode_chr_splitter(text) if chr ]

def tokenizeByMecabWrapper(untokenized):
    mecab_neologd_wrapper = JapaneseTokenizer.MecabWrapper(dictType='neologd')
    return mecab_neologd_wrapper.tokenize(untokenized).convert_list_object()

def pad_sentence(charList, sampleLength, padding_word="♫"):
    numPadding = sampleLength - len(charList)
    newCharList = charList + ["♫"] * numPadding
    return newCharList

def createPaddedList(sentenceFile,sampleLengthTok, sampleLengthChar, paddedTokListFile, paddedCharListFile, paddedTokLists, paddedCharLists):
    sentence = sentenceFile.readline().rstrip()
    if not sentence == "":
        paddedTokList = pad_sentence(tokenizeByMecabWrapper(sentence.replace(" ", "").replace("　", "").replace("。", "")), sampleLengthTok)
        paddedCharList = pad_sentence(split_unicode_chrs(sentence.replace(" ", "").replace("　", "").replace("。", "")), sampleLengthChar)
        paddedTokListFile.write(" ".join(paddedTokList) + "\n")
        paddedTokLists.append(numpy.array(paddedTokList))
        paddedCharListFile.write(" ".join(paddedCharList) + "\n")
        paddedCharLists.append(numpy.array(paddedCharList))
    while sentence:
        sentence = sentenceFile.readline().rstrip()
        if not sentence == "":
            paddedTokList = pad_sentence(
                tokenizeByMecabWrapper(sentence.replace(" ", "").replace("　", "").replace("。", "")), sampleLengthTok)
            paddedCharList = pad_sentence(
                split_unicode_chrs(sentence.replace(" ", "").replace("　", "").replace("。", "")), sampleLengthChar)
            paddedTokListFile.write(" ".join(paddedTokList) + "\n")
            paddedTokLists.append(numpy.array(paddedTokList))
            paddedCharListFile.write(" ".join(paddedCharList) + "\n")
            paddedCharLists.append(numpy.array(paddedCharList))
    return  paddedTokLists, paddedCharLists

def buildVocab(paddedLists,vocabFile, vocabInvFile):
    word_counts = Counter(itertools.chain(*paddedLists))
# Mapping from index to word
    vocabInv = [x[0] for x in word_counts.most_common()]
# Mapping from word to index
    vocab = {x: i for i, x in enumerate(vocabInv)}
    pickle.dump(vocab, vocabFile)
    pickle.dump(vocabInv, vocabInvFile)
    return vocab

# Maps sentences to vectors based on a vocabulary (problem with unknown words addressed)
def buildIndicesList(vocab, tokOrCharLists, indicesListFile, sampleLength, unknown):
    patSpace = re.compile(r"\s+", re.DOTALL)
    indicesLists = []
    for tcList in tokOrCharLists:
        indicesList = []
        for tc  in tcList:
            try:
                indicesList.append(str(vocab[tc]))
            except KeyError:
                indicesList.append(str(vocab[unknown]))
                print ("key error"+tc)
                pass
        toBeWritten = " ".join(indicesList)
        if len(patSpace.split(toBeWritten)) == sampleLength:
            indicesListFile.write(toBeWritten+"\n")
            indicesLists.append(toBeWritten)
        else:
            print ("sequence too long")
            print(len(patSpace.split(toBeWritten)))
            print (toBeWritten)
    return indicesLists

with codecs.open("ideasheet_test.csv", mode = "r", encoding="utf8") as csvIn:
    ideas = csvIn.readlines()

if __name__ == '__main__':
    symReportDict = {}
    ideaDict = {}
    patSym = re.compile(r".+?(\d\d\d\d).+", re.DOTALL)
    patComma = re.compile(r",", re.DOTALL)
    patPos = re.compile(r"L+|S", re.DOTALL)
    patLong = re.compile(r"L+", re.DOTALL)
    patSpace = re.compile(r"\s+", re.DOTALL)
    uniqRepPosLineFile = codecs.open("uniqRepPosLineBig.txt", mode="w", encoding="utf8")
    uniqRepNegLineFile = codecs.open("uniqRepNegLineBig.txt", mode="w", encoding="utf8")
    mbox = mailbox.mbox('ichiyoshi.mbox')
    vocabTokFile = open("vocabBigTok", 'wb')
    vocabInvTokFile = open("vocabInvBigTok", 'wb')
    vocabCharFile = open("vocabBigChar", 'wb')
    vocabInvCharFile = open("vocabInvBigChar", 'wb')
    paddedPosTokListFile = codecs.open("posListBigTok.txt", mode="w", encoding="utf8")
    paddedNegTokListFile = codecs.open("negListBigTok.txt", mode="w", encoding="utf8")
    paddedPosCharListFile = codecs.open("posListBigChar.txt", mode="w", encoding="utf8")
    paddedNegCharListFile = codecs.open("negListBigChar.txt", mode="w", encoding="utf8")
    posTokIndicesListFile = open("posIndicesListBigTok.txt", "w")
    negTokIndicesListFile = open("negIndicesListBigTok.txt", "w")
    posCharIndicesListFile = open("posIndicesListBigChar.txt", "w")
    negCharIndicesListFile = open("negIndicesListBigChar.txt", "w")
    debugFile = codecs.open("debug.txt", mode="w", encoding="utf8")
    repPos = ""
    repNeg = ""

# extract reports from mails with symbols in subject lines and created a dictionary with symbols as keys
    for message in mbox:
        try:
            subject = decode_header(message["subject"])[0][0].decode(decode_header(message["subject"])[0][1])
            if patSym.match(subject):
                symbol = patSym.match(subject).groups()[0]
                message.set_charset("iso2022_jp")
                body = message.get_payload(decode=True).decode("iso2022_jp")
                msgTime = mktime(parsedate(message["Date"]))
#                print ("msgTime:" + str(msgTime) + "\n")
                if not symbol in symReportDict.keys():
                    symReportDict[symbol] = {msgTime: body}
                else:
                    symReportDict[symbol][msgTime] = body
        except Exception:
            pass

# extract recomendations from csv and created a dictionary with symbols as keys
    for i, idea in enumerate(ideas):
        if len(patComma.split(idea.rstrip())) == 3 and not i == 0:
            symbol = patComma.split(idea.rstrip())[1]
            ideaTime = mktime(strptime(patComma.split(idea.rstrip())[0],"%Y%m%d"))
            pos = patComma.split(idea.rstrip())[2]
            if not symbol in ideaDict.keys():
                ideaDict[symbol] = {ideaTime: pos}
            else:
                if not ideaTime in ideaDict[symbol].keys():
                    ideaDict[symbol][ideaTime] = pos
                else:
# remove recommendations at the same time that contradict each other
                    if not ideaDict[symbol][ideaTime] == pos:
                        ideaDict[symbol] = removeKey(ideaDict[symbol], ideaTime)

# extract reports about symbols covered in ideasheet_test.csv first
# this part is the same as ichiYoshi.py
    for si,pair in ideaDict.items():
# store recommendations
        pStore = []
        for ti,p in sorted(pair.items()):
            if si in symReportDict.keys():
# when no recommendations
                if len(pStore) == 0:
# loop over reports  about a symbol released earlier than the recommendation about the symbol
                    for m in [m for tr, m in symReportDict[si].items() if tr <= ti]:
# if recommendation is "long", put that report in the collection of positive reports
                        if patLong.match(p):
                            repPos += m
# otherwise, put it in the collection of negative reports
                        else:
                            repNeg += m
# when previous recommendations exist
                else:
# loop over reports about a symbol released earlier than the recommendation about the symbol but not earlier than the last recommendation about the same symbol
                    for m in [m for tr, m in symReportDict[si].items() if tr <= ti and tr > pStore[-1][0]]:
                        if patLong.match(p):
                            repPos += m
                        else:
                            repNeg += m
            pStore.append((ti, p))

# then classify all reports left over as negative examples
# loop over all reports extracted from mails
    for sr, pair in  symReportDict.items():
        for tr, m in sorted(pair.items()):
# if the symbol covered by a report is found in ideasheet_test.csv
            if sr in ideaDict.keys() and not len(ideaDict[sr].items()) == 0 :
                debugFile.write (str(sr)+"\n")
                debugFile.write (str(tr)+"\n")
                debugFile.write (str(ideaDict[sr].items())+"\n")
# if the report is released later than all recommendations about the symbol
                if not tr <= sorted(ideaDict[sr].items())[-1][0]:
                    debugFile.write("hit: latest idea:"+str(sorted(ideaDict[sr].items())[-1][0])+"\n")
                    debugFile.write("hit: report time:" + str(tr)+"\n")
# put it in the collection of negative reports
                    repNeg += m
# if the symbol covered by a report is not found in ideasheet_test.csv
            else:
# put it in the collection of negative reports
                repNeg += m

# split report by newline or period, remove empty line, links and lines exist in both positive and negative reports
    uniqRepPosLines = [unicodedata.normalize('NFKC', rpLine)  for rpLine in re.split("\n|。", repPos) if not rpLine in  re.split("\n|。", repNeg) and not rpLine.startswith("http") and not rpLine.strip() == ""]
    uniqRepNegLines = [unicodedata.normalize('NFKC', rpLine)  for rpLine in  re.split("\n|。", repNeg) if not rpLine in  re.split("\n|。", repPos) and not rpLine.startswith("http") and not rpLine.strip() == ""]

    lenStoreTok = []
    lenStoreChar = []
# loop over training data to determine input dimension for neural network
    for rpLine in uniqRepNegLines:
        lenStoreTok.append(len(tokenizeByMecabWrapper(rpLine)))
        lenStoreChar.append(len(split_unicode_chrs(rpLine)))
        uniqRepNegLineFile.write(rpLine.rstrip()+"\n")
    for rpLine in uniqRepPosLines:
        lenStoreTok.append(len(tokenizeByMecabWrapper(rpLine)))
        lenStoreChar.append(len(split_unicode_chrs(rpLine)))
        uniqRepPosLineFile.write(rpLine.rstrip() + "\n")

    inputLenTokMean = numpy.mean(numpy.array(lenStoreTok))
    inputLenTokStd = numpy.std(numpy.array(lenStoreTok))
#one dimesnion for token embedding
    sampleLengthTok = int(inputLenTokMean + 2*inputLenTokStd)
    print (sampleLengthTok)

    inputLenCharMean = numpy.mean(numpy.array(lenStoreChar))
    inputLenCharStd = numpy.std(numpy.array(lenStoreChar))
#one dimension for character embedding
    sampleLengthChar = int(inputLenCharMean + 2 * inputLenCharStd)
    print (sampleLengthChar)

    uniqRepPosLineFile.close()
    uniqRepNegLineFile.close()
    uniqRepPosLineFile = codecs.open("uniqRepPosLineBig.txt", mode="r", encoding="utf8")
    uniqRepNegLineFile = codecs.open("uniqRepNegLineBig.txt", mode="r", encoding="utf8")

# make input lines (of the same type) have the same length by padding lines shorter than the input dimension
# input lines split into tokens are padded to the dimension for token embedding
# input lines split into characters are padded to the dimension for character embedding
    paddedPosTokLists, paddedPosCharLists = createPaddedList(uniqRepPosLineFile, sampleLengthTok, sampleLengthChar, paddedPosTokListFile, paddedPosCharListFile, [], [])
    paddedNegTokLists, paddedNegCharLists = createPaddedList(uniqRepNegLineFile, sampleLengthTok, sampleLengthChar,  paddedNegTokListFile, paddedNegCharListFile, [], [])

    paddedPosTokListFile.close()
    paddedNegTokListFile.close()
    paddedPosCharListFile.close()
    paddedNegCharListFile.close()

# create vocabulary
#    vocabTok = buildVocab(paddedPosTokLists+paddedNegTokLists, vocabTokFile, vocabInvTokFile)
#    vocabChar = buildVocab(paddedPosCharLists + paddedNegCharLists, vocabCharFile, vocabInvCharFile)
    vocabTok = buildVocab(paddedNegTokLists, vocabTokFile, vocabInvTokFile)
    vocabChar = buildVocab(paddedNegCharLists, vocabCharFile, vocabInvCharFile)

# turn list of tokens into list of indices
# positive examples and negative examples are kept separate
    buildIndicesList(vocabTok, paddedPosTokLists, posTokIndicesListFile, sampleLengthTok, "♫")
    buildIndicesList(vocabTok, paddedNegTokLists, negTokIndicesListFile, sampleLengthTok, "♫")

# turn list of characters into list of indices
    buildIndicesList(vocabChar, paddedPosCharLists, posCharIndicesListFile, sampleLengthChar, "♫")
    buildIndicesList(vocabChar, paddedNegCharLists, negCharIndicesListFile, sampleLengthChar, "♫")

    vocabTokFile.close()
    vocabInvTokFile.close()
    vocabCharFile.close()
    vocabInvCharFile.close()
    uniqRepPosLineFile.close()
    uniqRepNegLineFile.close()
    posTokIndicesListFile.close()
    negTokIndicesListFile.close()
    posCharIndicesListFile.close()
    negCharIndicesListFile.close()