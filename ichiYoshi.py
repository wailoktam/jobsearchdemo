import mailbox
import codecs
import email
import re
import numpy
import itertools
import pickle

from email.utils import parsedate
from time import mktime,strptime
from email.header import decode_header
from collections import Counter
from itertools import chain

def removeKey(oldDict, key):
    newDict = dict(oldDict)
    del newDict[key]
    return newDict

def split_unicode_chrs(text):
    pat_unicode_chr_splitter = re.compile('(?s)((?:[\ud800-\udbff][\udc00-\udfff])|.)').split
    return [ chr for chr in pat_unicode_chr_splitter(text) if chr ]

def pad_sentence(charList, sampleLength, padding_word="♫"):
    numPadding = sampleLength - len(charList)
    newCharList = charList + ["♫"] * numPadding
    return newCharList

def createPaddedCharList(sentenceFile,sampleLength,  paddCharListFile, paddedCharLists):
    sentence = sentenceFile.readline().rstrip()
    if not sentence == "":
        paddedCharList = pad_sentence(split_unicode_chrs(sentence.replace(" ", "").replace("　", "").replace("。", "")), sampleLength)
        paddCharListFile.write(" ".join(paddedCharList) + "\n")
        paddedCharLists.append(numpy.array(paddedCharList))
    while sentence:
        sentence = sentenceFile.readline().rstrip()
        if not sentence == "":
            paddedCharList = pad_sentence(split_unicode_chrs(sentence.replace(" ", "").replace("　", "").replace("。", "")), sampleLength)
            paddCharListFile.write(" ".join(paddedCharList) + "\n")
            paddedCharLists.append(numpy.array(paddedCharList))
    return  paddedCharLists

def buildVocab(paddedCharLists,vocabFile, vocabInvFile):
    word_counts = Counter(itertools.chain(*paddedCharLists))
# Mapping from index to word
    vocabInv = [x[0] for x in word_counts.most_common()]
# Mapping from word to index
    vocab = {x: i for i, x in enumerate(vocabInv)}
    pickle.dump(vocab, vocabFile)
    pickle.dump(vocabInv, vocabInvFile)
    return vocab

def buildIndicesList(vocab, charLists, indicesListFile, sampleLength):
#Maps sentencs to vectors based on a vocabulary.
    patSpace = re.compile(r"\s+", re.DOTALL)
    indicesLists = []
    for charList in charLists:
        indicesList = []
        for c  in charList:
            try:
                indicesList.append(str(vocab[c]))
            except KeyError:
                indicesList.append(str(vocab["♫"]))
                print ("key error"+c)
                pass
        toBeWritten = " ".join(indicesList)
        if len(patSpace.split(toBeWritten)) == sampleLength:
            indicesListFile.write(toBeWritten+"\n")
            indicesLists.append(toBeWritten)
        else:
            print ("chars not sequence length")
            print(len(patSpace.split(toBeWritten)))
            print (toBeWritten)
    return indicesLists

with codecs.open("ideasheet_test.csv", mode = "r", encoding="utf8") as csvIn:
    ideas = csvIn.readlines()

if __name__ == '__main__':
    symReportDict = {}
    ideaDict = {}
    patSym = re.compile(r".+?(\d\d\d\d).+", re.DOTALL)
    patComma = re.compile(r",", re.DOTALL)
    patPos = re.compile(r"L+|S", re.DOTALL)
    patLong = re.compile(r"L+", re.DOTALL)
    patSpace = re.compile(r"\s+", re.DOTALL)
    uniqRepPosLineFile = codecs.open("uniqRepPosLine.txt", mode="w", encoding="utf8")
    uniqRepNegLineFile = codecs.open("uniqRepNegLine.txt", mode="w", encoding="utf8")
    mbox = mailbox.mbox('ichiyoshi.mbox')
    vocabFile = open("vocab", 'wb')
    vocabInvFile = open("vocabInv", 'wb')
    paddedPosCharListFile = codecs.open("posCharList.txt", mode="w", encoding="utf8")
    paddedNegCharListFile = codecs.open("negCharList.txt", mode="w", encoding="utf8")
    posIndicesListFile = open("posIndicesList.txt", "w")
    negIndicesListFile = open("negIndicesList.txt", "w")
    repPos = ""
    repNeg = ""

#extract reports from mails with symbols in subject lines and created a dictionary with symbols as keys
    for message in mbox:
        try:
            subject = decode_header(message["subject"])[0][0].decode(decode_header(message["subject"])[0][1])
            if patSym.match(subject):
                symbol = patSym.match(subject).groups()[0]
                message.set_charset("iso2022_jp")
                body = message.get_payload(decode=True).decode("iso2022_jp")
                msgTime = mktime(parsedate(message["Date"]))
                if not symbol in symReportDict.keys():
                    symReportDict[symbol] = {msgTime: body}
                else:
                    symReportDict[symbol][msgTime] = body
        except Exception:
            pass

#extract recomendations from csv and created a dictionary with symbols as keys
    for i, idea in enumerate(ideas):
        if len(patComma.split(idea.rstrip())) == 3 and not i == 0:
            symbol = patComma.split(idea.rstrip())[1]
            ideaTime = mktime(strptime(patComma.split(idea.rstrip())[0],"%Y%m%d"))
            pos = patComma.split(idea.rstrip())[2]
            if not symbol in ideaDict.keys():
                ideaDict[symbol] = {ideaTime: pos}
            else:
                if not ideaTime in ideaDict[symbol].keys():
                    ideaDict[symbol][ideaTime] = pos
                else:
#remove recommendations at the same time that contradict each other
                    if not ideaDict[symbol][ideaTime] == pos:
                        ideaDict[symbol] = removeKey(ideaDict[symbol], ideaTime)

#extract only reports about symbols covered in ideasheet_test.csv
#reports about symobls not covered in ideasheet_test.csv are not considered negative examples
    for si,pair in ideaDict.items():
#store recommendations
        pStore = []
        for ti,p in sorted(pair.items()):
            if si in symReportDict.keys():
#when no recommendations stroed
                if len(pStore) == 0:
#loop over reports  about a symbol released earlier than the recommendation about the symbol
                    for m in [m for tr, m in symReportDict[si].items() if tr <= ti]:
#if recommendation is "long", put that report in the collection of positive reports
                        if patLong.match(p):
                            repPos += m
#otherwise, put it in the collection of negative reports
                        else:
                            repNeg += m
#when previous recommendations exist
                else:
#loop over reports about a symbol released earlier than the recommendation about the symbol but not earlier than the last recommendation about the same symbol
                    for m in [m for tr, m in symReportDict[si].items() if tr <= ti and tr > pStore[-1][0]]:
                        if patLong.match(p):
                            repPos += m
                        else:
                            repNeg += m
            pStore.append((ti, p))

#split report by newline or period, remove empty line, links and lines exist in both positive and negative reports
    uniqRepPosLines = [rpLine  for rpLine in re.split("\n|。", repPos) if not rpLine in  re.split("\n|。", repNeg) and not rpLine.startswith("http") and not rpLine.strip() == ""]
    uniqRepNegLines = [rpLine  for rpLine in  re.split("\n|。", repNeg) if not rpLine in  re.split("\n|。", repPos) and not rpLine.startswith("http") and not rpLine.strip() == ""]

    lenStore = []
#loop over training data to determine input dimension for neural network
    for rpLine in uniqRepNegLines:
        lenStore.append(len(split_unicode_chrs(rpLine)))
        uniqRepNegLineFile.write(rpLine.rstrip()+"\n")
    for rpLine in uniqRepPosLines:
        lenStore.append(len(split_unicode_chrs(rpLine)))
        uniqRepPosLineFile.write(rpLine.rstrip() + "\n")
    inputLenMean = numpy.mean(numpy.array(lenStore))
    inputLenStd = numpy.std(numpy.array(lenStore))
    sampleLength = int(inputLenMean + 2*inputLenStd)
    print (sampleLength)

    uniqRepPosLineFile.close()
    uniqRepNegLineFile.close()
    uniqRepPosLineFile = codecs.open("uniqRepPosLine.txt", mode="r", encoding="utf8")
    uniqRepNegLineFile = codecs.open("uniqRepNegLine.txt", mode="r", encoding="utf8")

#make all input lines have the same length by padding lines shorter than the input dimension
    paddedPosCharLists = createPaddedCharList(uniqRepPosLineFile, sampleLength,  paddedPosCharListFile, [])
    paddedNegCharLists = createPaddedCharList(uniqRepNegLineFile, sampleLength,  paddedNegCharListFile, [])

    paddedPosCharListFile.close()
    paddedNegCharListFile.close()

#create vocabulary
    vocab = buildVocab(paddedPosCharLists+paddedNegCharLists, vocabFile, vocabInvFile)

#turn list of characters into list of indices
#positive examples and negative examples are kept separate
    buildIndicesList(vocab, paddedPosCharLists, posIndicesListFile, sampleLength)
    buildIndicesList(vocab, paddedNegCharLists, negIndicesListFile, sampleLength)

    vocabFile.close()
    vocabInvFile.close()
    uniqRepPosLineFile.close()
    uniqRepNegLineFile.close()
    posIndicesListFile.close()
    negIndicesListFile.close()




