# -*- coding: utf-8 -*-
import os
import re
import numpy
import sklearn
import random
import pickle
import codecs
from ichiYoshi import createPaddedCharList, buildIndicesList
from sklearn.model_selection import StratifiedKFold
from gensim.models import word2vec
from keras.models import Sequential, Model
from keras.models import load_model
from keras.layers.convolutional import Conv1D
from keras.layers.pooling import MaxPooling1D
from keras.layers import Merge
from keras.layers.merge import Concatenate, concatenate
from keras.layers import Activation, Dense, Dropout, Embedding, Flatten, Input, LSTM, Bidirectional
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from keras.optimizers import SGD
from keras.utils import plot_model

#choose an action first
ACTION = "train & cross-validate"
#ACTION = "predict"

def loadDataKfold(indicesListFile,sign, k):
    indicesLines = indicesListFile.readlines()
    x = []
    y = []
    for indicesLine  in indicesLines:
        indices = [int(i) for i in indicesLine.split()]
        x.append(indices)
        y.append(sign)
    folds = list(StratifiedKFold(n_splits=k, shuffle=True, random_state=1).split(x, y))
    return folds, numpy.array(x)

def adjustRatio(xPlusValidCv, xMinusValidCv, pNRatio):
    xStore = []
    yStore = []
    xPlus4Pop = xPlusValidCv.tolist()
    xMinus4Pop = xMinusValidCv.tolist()
    while 1:
        if random.random() > pNRatio:
            if not len(xMinus4Pop) == 0:
                indices = xMinus4Pop.pop()
                label = 0
            else:
                break
        else:
            if not len(xPlus4Pop) == 0:
                indices = xPlus4Pop.pop()
                label = 1
            else:
                break
        xStore.append(indices)
        yStore.append(label)
    return numpy.array(xStore), numpy.array(yStore)

#feed data to neural network while keeping the ratio of positive examples to negative examples at pNRatio
def generateArraysFromLists(xPlus,xMinus,pNRatio,stepSize):
    stepCount = 0
    xStore = []
    yStore = []
    xPlus4Pop = xPlus.tolist()
    xMinus4Pop = xMinus.tolist()
    while 1:
        if random.random() > pNRatio:
            if not len(xMinus4Pop) == 0:
                indices = xMinus4Pop.pop()
                label = 0
            else:
                xMinus4Pop = xMinus.tolist()
                indices = xMinus4Pop.pop()
                label = 0
        else:
            if not len(xPlus4Pop) == 0:
                indices = xPlus4Pop.pop()
                label = 1
            else:
                xPlus4Pop = xPlus.tolist()
                indices = xPlus4Pop.pop()
                label = 1
        xStore.append(indices)
        yStore.append(label)
        stepCount += 1
        if stepCount == stepSize:
            stepCount = 0
            xOut = numpy.array(xStore)
            xStore = []
            yOut = numpy.array(yStore)
            yStore = []
            yield xOut, yOut

def createModel(vocab, vocabInv, charVecModel, sampleLength):
    embedding_dim = 20
    dropout_prob = (0.25, 0.5)
    hidden_dims = 200
    embeddingWeights = [numpy.array([charVecModel[w] if w in charVecModel \
                                          else numpy.random.uniform(-0.25, 0.25, charVecModel.vector_size) \
                                      for w in vocabInv])]
    embedLayer = Sequential()
    embedLayer.add(Embedding(len(vocab), embedding_dim, input_length=sampleLength, weights=embeddingWeights))
    embedLayer.add(Bidirectional(LSTM(10, return_sequences=True)))
    model = Sequential()
    model.add(embedLayer)
    model.add(Dense(hidden_dims))
    model.add(Dropout(dropout_prob[1]))
    model.add(Flatten())
    model.add(Activation('relu'))
    model.add(Dense(1))
    model.add(Activation('sigmoid'))
    opt = SGD(lr=0.01, momentum=0.80, decay=1e-6, nesterov=True)
    model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['accuracy'])
    plot_model(model, to_file="archHayate.png", show_shapes=True)
    return model

if __name__ == '__main__':
#interim output from creating vocabulary is  written out as files and read againto avoid memory error
#load vocab
    with open("vocabInv", mode='rb') as f:
        vocabInv = pickle.load(f)
    with open("vocab", mode='rb') as f:
        vocab = pickle.load(f)

#load pre-trained character embedding due to time constraint
    charVecModel = word2vec.Word2Vec.load("/home/labmanager/extraData/modelsCPlusCW24/20features_1minwords_10contextC")

#interim output from turning charaters into indices is  written out as files and read againto avoid memory error
#load indexed training data
    posIndicesListFile = open("posIndicesList.txt", "r")
    negIndicesListFile = open("negIndicesList.txt", "r")

#positive negative example ratio used for training and validation
    pNRatio = 0.5

#set to 1 due to time constraint
    numEpoch = 1

    stepsPerEpoch = 5

#k-fold cross-validation
    k = 5

    modelName = "charVec"

#loop over training data to determine:
#   input dimension for neural network: sampleLength
#   step size used when feeding data to neural network: step size
    posSamples = posIndicesListFile.readlines()
    posSampleSize = len(posSamples)
    print ("positive sample size:" + str(posSampleSize))
    negSampleSize = len(negIndicesListFile.readlines())
    print ("negative sample size:" + str(negSampleSize))

    sampleLength = len(posSamples[0].split())
    print ("sample length:" + str(sampleLength))

#ratio of positve example vs negative example are adjusted. hence the adjusted sample size
    if posSampleSize >= negSampleSize:
        adjSampleSize = (k - 1) * posSampleSize / k / pNRatio
    elif negSampleSize > posSampleSize:
        adjSampleSize = (k - 1) * negSampleSize / k / (1 - pNRatio)

    stepSize = int(adjSampleSize / stepsPerEpoch)
    print ("step size:" + str(stepSize))

    negIndicesListFile.close()
    posIndicesListFile.close()
    posIndicesListFile = open("posIndicesList.txt", "r")
    negIndicesListFile = open("negIndicesList.txt", "r")

    if  ACTION == "train & cross-validate":
        model = createModel(vocab, vocabInv, charVecModel, sampleLength)
#split positive examples into training and validation set by k-1 to 1
        posFolds, xPlus = loadDataKfold(posIndicesListFile, 1, k)
#split negative examples into training and validation set by k-1 to 1
        negFolds, xMinus = loadDataKfold(negIndicesListFile, 0, k)
        for i, (posTrainValIdx,  negTrainValIdx) in enumerate(zip(posFolds, negFolds)):
            print('\nFold ', i)
            posTrainIdx = posTrainValIdx[0]
            posValIdx = posTrainValIdx[1]
            negTrainIdx = negTrainValIdx[0]
            negValIdx = negTrainValIdx[1]
            xPlusTrainCv = xPlus[posTrainIdx]
            xPlusValidCv = xPlus[posValIdx]
            xMinusTrainCv = xMinus[negTrainIdx]
            xMinusValidCv = xMinus[negValIdx]
#adjust ratio of positive examples and negative examples in the validation set based on pNRatio
            xValidCv, yValidCv = adjustRatio(xPlusValidCv, xMinusValidCv, pNRatio)
            model.fit_generator(
                generateArraysFromLists(xPlusTrainCv,xMinusTrainCv,pNRatio,stepSize),
                steps_per_epoch=stepsPerEpoch,
                shuffle=True,
                epochs=numEpoch,
                verbose=1,
                validation_data = (xValidCv, yValidCv))
            print(model.evaluate(xValidCv, yValidCv))
        model.save_weights("weight"+modelName+".hdf5", overwrite=True)
        model.model.save("model"+modelName+".hdf5", overwrite=True)

    elif ACTION == "predict":
        modelName = "charVec"
        model = load_model("model"+modelName+".hdf5")
        model.load_weights("weight"+modelName+".hdf5")
#put the input for prediction here
        input4Predict =  codecs.open("input4Predict.txt", mode="r", encoding="utf8")
#processed like training data
        inputLineFile = codecs.open("input4PredictLine.txt", mode="w", encoding="utf8")
        paddedInCharListFile = codecs.open("input4PredictCharList.txt", mode="w", encoding="utf8")
        indicesList4PredictFile = open("indicesList4Predict.txt", "w")
        for i in re.split("\n|。", input4Predict.read()):
            if not i.startswith("http") and not i.strip() == "":
                inputLineFile.write(i.rstrip()+"\n")
        inputLineFile.close()
        inputLineFile = codecs.open("input4PredictLine.txt", mode="r", encoding="utf8")
        paddedInCharLists = createPaddedCharList(inputLineFile, sampleLength, paddedInCharListFile, [])
        indicesLists4Predict =buildIndicesList(vocab, paddedInCharLists, indicesList4PredictFile, sampleLength)
        x = []
        for indicesLine in indicesLists4Predict:
            indices = [int(i) for i in indicesLine.split()]
            x.append(indices)
#return average score of lines in the submitted report
        score = numpy.mean(numpy.array(model.predict(numpy.array(x))))
        print ("score:"+str(score))
        if score >= 0.5:
            print ("long")
        else:
            print ("short or do nothing")
    posIndicesListFile.close()
    negIndicesListFile.close()
